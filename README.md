# nmap: scripting demo (e.g. pgsql-brute)

This is a demo showing nmap's scripting abilities by using pgsql-brute to bruteforce out way into a badly configured postgres database. This demo has been created as part of a `hacking-tool` demonstation exercise. It requires a local installation of nmap as well as docker for easy and repeatable setup of the postgres database.

## Includes

This repository includes `usernames.txt` and `rockyou_first_256.txt` files that contain the usernames and passwords in use by this demo. This has been done to dramatically speed up the demo by not having to bruteforce too many diffrent combinations.


## Setup

In these steps we will create the postgres database with an integrated root superuser account, aswell as an application account to create a more realistic scenario.

1. Create and start PostgreSQL container on port 5432

```
docker run -itd --rm --name thm-demo-postgres -e POSTGRES_USER=root -e POSTGRES_PASSWORD=cupcake1 -p 5432:5432 postgres:10
```

2. Create 'application' user with insecure password
```
docker exec -it thm-demo-postgres /bin/sh -c "echo \"CREATE USER application WITH PASSWORD 'snuffles';\" | psql"
```

## Execution

Start nmap script with username and password files

```
nmap -d --script=pgsql-brute --script-args passdb=./rockyou_first_256.txt,userdb=./usernames.txt 127.0.0.1
```

## Cleanup

This command will stop / remove the postgres container
```
docker stop thm-demo-postgres
```